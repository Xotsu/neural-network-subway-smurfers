// Start the game over
function resetGame() {
  if(score> highScore){
    highScore = score;
  }
  score = 0;
  // Resetting best player score to 0

  blocks = [];
  blockCombos = []
  for(i = 0; i<masterBlockCombos.length; i++){
    blockCombos.push(masterBlockCombos[i])
  }
  selection.push(random(blockCombos))
  blocks.push(new Block(selection[0], 0))
  blockCombos.splice(blockCombos.indexOf(selection[0]), 1)
  selection = [];
}

// Create the next generation
function nextGeneration() {
  resetGame();
  // Normalize the fitness values 0-1
  console.log(totalPlayers[0])
  normalizeFitness(totalPlayers);
  console.log(totalPlayers[0])
  // Generate a new set of players
  activePlayers = generate(totalPlayers);
  // Copy those players to another array
  totalPlayers = activePlayers.slice();

}

// Generate a new population of players
function generate(oldPlayers) {
  let newPlayers = [];
  for (let i = 0; i < oldPlayers.length; i++) {
    // Select a player based on fitness
    let player = poolSelection(oldPlayers);
    newPlayers[i] = player;
  }
  return newPlayers;
}

// Normalize the fitness of all players
function normalizeFitness(players) {
  ////////////////////////////////////////////////////////////////////
  // Make score exponentially better?
  for (let i = 0; i < players.length; i++) {
    players[i].score = pow(players[i].score, 2);
    console.log(players[i].score)
  }
  ////////////////////////////////////////////////////////////////////

  // Add up all the scores
  let sum = 0;
  for (let i = 0; i < players.length; i++) {
    sum += players[i].score;
  }
  console.log(sum)
  // Divide by the sum
  for (let i = 0; i < players.length; i++) {
    players[i].fitness = players[i].score / sum;
  }
}


// An algorithm for picking one player from an array
// based on fitness
function poolSelection(players) {
  // Start at 0
  let index = 0;
  // Pick a random number between 0 and 1
  let r = random(1);

  // Keep subtracting probabilities until you get less than zero
  // Higher probabilities will be more likely to be fixed since they will
  // subtract a larger number towards zero
  while (r > 0) {
    r -= players[index].fitness;
    // And move on to the next
    index += 1;
  }

  // Go back one
  index -= 1;

  //console.log(players[index].copy())
  return players[index].copy();
}
