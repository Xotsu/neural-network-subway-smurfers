class LayerLink{
    constructor(prevNode_count,node_count){
      if(prevNode_count instanceof LayerLink){
        this.weights = prevNode_count.weights.copy()
        this.bias = prevNode_count.bias.copy()
      }else{
        this.weights = new Matrix(node_count, prevNode_count);
        this.bias = new Matrix(node_count, 1);
        this.weights.randomize();
        this.bias.randomize();
      }
    }
    // //Use this to assign weights from an entity of a generation you want to copy
    // updateWeights(weights){
    //     this.weights = weights;
    // }
    getWeights(){
        return this.weights;
    }
    getBias(){
        return this.bias;
    }
    add(deltaWeight, bias){
        this.weights.add(deltaWeight);
        this.bias.add(bias);
    }
    copy(){
      return new LayerLink(this);
    }
}
