class Block {

  constructor(arr, y) {
    this.arr = arr
    this.y = y
    this.color = color(random(255), random(255), random(255))
  }

  display() {
    push()
    fill(this.color)
    // console.log(this.arr)
    // console.log(selection)
    for (let i = 0; i<3; i++){
      if (this.arr[i] == 1){
        rect(i*150+50, this.y, 50, 50)
      }
    }
    pop()
  }

  update() {
    this.y -= 2
    if (this.y > height) {
      blocks.splice(blocks.indexOf(this), 1)
    }
    this.y += 20
  }

  hit(player){
    for (let i = 0; i<3; i++){
      if (this.arr[i] == 1){
         if (collideRectRect(i*150+50, this.y, 50, 50,player.x,player.y,50,50)){
          return true
         }
      }
    }
    return false




    // if (collideRectRect(i*150+50, this.y, 50, 50,player.x,player.y,50,50)){
    //   return true
    // }else{
    //   return false
    // }




    // for (let i = 0; i<this.arr.length; i++){
    //   if (this.arr[i] == 1){
    //      if (collideRectRect(i*150+50, this.y, 50, 50,playerX,playerY,50,50)){
    //       gameState = false;
    //      }
    //   }
    // }

  }
}
