//COMPLETED 27/08/2019
//EZ
// let gameState = true
let pos = [50, 200, 350]
// let playerX = 200
// let playerY = 430
let masterBlockCombos = [[1,0,0],[1,1,0],[1,0,1],[0,1,1],[0,0,1],[0,1,0]]
let blockCombos = [[1,0,0],[1,1,0],[1,0,1],[0,1,1],[0,0,1],[0,1,0]]
let blocks = []
let temp = []
let selection = []
let score = 0
let players = []
let totalPopulation = 1000
let activePlayers = []
let totalPlayers = []
let highScore = 0
let genCount = 0
const gap = 200

//spawn blocks in 150 intervals there should be

//2 player inputs closest block x and player x

function setup() {
  createCanvas(450, 500)
  createPopulation()
  initial()

}




function draw() {
  //console.log(Math.floor(random(2))) 0 or 1 generator
  background(50)
  score++
  updateEntities()
  runPlayers()

}

// function keyPressed() {
//   if (keyCode === LEFT_ARROW && gameState && !(playerX - 150 < 0)) {
//     playerX -= 150
//   } if (keyCode === RIGHT_ARROW && gameState && !(playerX + 150 > width)) {
//     playerX += 150
//   }
// }


function initial(){
  selection.push(random(blockCombos))
  blocks.push(new Block(selection[0], 0))
  blockCombos.splice(blockCombos.indexOf(selection[0]), 1)
  selection = []
}

function createPopulation(){
  for (let i = 0; i < totalPopulation; i++) {
    let player = new Player();
    activePlayers[i] = player;
    totalPlayers[i] = player;
  }
}

function addBlocks() {
  //Creates array of existing values, chooses value at top of screen - gap and adds new element to that y location
  if(blockCombos.length === 0){
    //console.log("now")
    //console.log(blockCombos)

    for(i = 0; i<masterBlockCombos.length; i++){
      blockCombos.push(masterBlockCombos[i])
      //console.log("pushing")
    }
    //console.log(blockCombos)
  }
  for (b of blocks) {
    temp.push(b.y)
  }
  //Selects
  //console.log(blockCombos)
  selection.push(random(blockCombos))
  blocks.push(new Block(selection[0], min(temp) - gap))
  blockCombos.splice(blockCombos.indexOf(selection[0]), 1)
  //console.log(blockCombos.length)
  selection = []
  temp = []
}

function updateEntities() {
  // //displaying player DONT NEED!!!!!!!!!
  // push()
  // //rectMode(CENTER)
  // rect(playerX, playerY, 50, 50)
  // pop()


  //console.log(blocks)
  if (blocks.length < 10) {
    addBlocks()
  }
  for (b of blocks) {
    b.display()
    b.update()
  }
  //console.log("pre pre thinking")
  for (p of activePlayers){
    //console.log("thinking")
    p.think(blocks);
    p.update();

    for (let j = 0; j < blocks.length; j++) {
      if (blocks[j].hit(p)) {
        //console.log("hit")
        activePlayers.splice(activePlayers.indexOf(p), 1);
        break;
      }
    }

  }
  push()
  fill(250)
  textSize(30)
  text(score, 10,30)
  text(activePlayers.length, 0, 90)
  pop()
  push()
  fill(250)
  textSize(30)
  text("Your High Score was: " + highScore, 10, 60)
  pop()
}

function runPlayers(){
  for (let i = 0; i < activePlayers.length; i++) {
    activePlayers[i].show();
  }
  if (activePlayers.length == 0) {
    nextGeneration()
    genCount++
    console.log(genCount)
    //console.log(activePlayers[0])
    //console.log(totalPlayers)
  }
}
