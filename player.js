
function mutate(x) {
  //console.log(x)
  if (random(1) < 0.1) {
    let offset = randomGaussian() * 0.5;
    let newx = x + offset;
    return newx;
  } else {
    return x;
  }
}



class Player{
  constructor(brain){
    this.x =200
    this.y = 430
    if (brain instanceof NeuralNetwork) {
      //console.log(brain)
      this.brain = brain.copy();
      //console.log(this.brain)
      //this.brain.mutate();
      this.brain.mutate(mutate);
      //console.log(this.brain)
      //everything should be working past this point with the .copy()
    } else {
      // takes in 5 inputs outputs 3 positions the one with highest percentage gets used
      this.brain = new NeuralNetwork([5, 5, 3]);
    }

    // Score is how many obsticles it has dodged so far (-9 as the scoring system adds a point whenever a new block is created
    //this is ot take into account two blocks at one level) therefore after startup and generation of the remaining blocks the score is 0
    this.score = 0;
    // Fitness is normalized version of score
    this.fitness = 0;
  }
  update(){
    this.score ++;
  }

  show() {
    rect(this.x, this.y, 50, 50);
  }

  left(){
    if(!(this.x - 150 < 0)) {
      this.x -= 150
    }
  }
  right(){
    if(!(this.x + 150 > width)){
      this.x += 150
    }
  }

  copy() {
    return new Player(this.brain);
  }


  think(blocks) {
    // Now create the inputs to the neural network
    //console.log(".")
    let inputs = [];
    // x position of closest pipe
    //first position state of closest block
    inputs[0] = blocks[0].arr[0]
    //second position state of closest block
    inputs[1] = blocks[0].arr[1]
    //third position stet of closest block
    inputs[2] = blocks[0].arr[2]
    //y position of the closest block between 0 and 1
    inputs[3] = blocks[0].y/height
    //player position between 0 and 1
    inputs[4] = this.x/width


    // Get the outputs from the network
    let action = this.brain.predict(inputs);
    // Decide to move left, right or not to move!
    if(action.indexOf(max(action)) == 0){
      this.left();
    }
    if(action.indexOf(max(action)) == 1){
      this.right();
    }
    if(action.indexOf(max(action)) == 2){
      //liteally nothing happens i put this in here so i would remember there are 3 outputs
    }
  }

}
